// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user controller
const orderController = require("../controllers/orderController")

router.post("/users/:userId/orders", orderController.createOrder);
router.get("/orders", orderController.getAllOrder);
router.get("/orders/:orderId", orderController.getOrderById);
router.put("/orders/:orderId", orderController.updateOrderById);
router.delete("/users/:userId/orders/:orderId", orderController.deleteOrderById);
//extra
router.get("/users/:userId/orders", orderController.getAllDetailOrderByUserId);
module.exports = router;